#encoding: utf-8
#language:pt

@2 @tela-grupoperda
Funcionalidade: Validar funcionamento Tela Grupo Perda

Contexto:
    Dado que eu esteja logado no sistema omni erp
    Quando eu acesso a tela do menu

# ACESSAR TELA FONTE ANALISE - EFETUA TESTE DE ACESSO A TELA
### TESTADO E FUNCIONANDO EM 09/05/2022
@acessar-tela-grupoperda
Cenario: Verificar os campos existentes na tela download upload
    E seleciono o <menu> e <submenu> e <subsubmenu>
    Entao eu espero visualizar o elemento <elementotela> na tela grupo perda  

Exemplos:
    |menu       |submenu       |subsubmenu   |elementotela    |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"Grupo de perda"|

# INCLUSÃO >>> CENÁRIO QUE EFETUA O CADASTRO DE NOVO GRUPO PERDA 
#              CLICA NO BOTÃO SALVAR
### TESTADO E FUNCIONANDO EM 09/05/2022
@tela-grupo-perda  @inclusao-grupo-perda
Cenario: Cadastrar registros na tabela grupo perda
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao da tela grupo de perda 'Novo Grupo Perda'
    E insiro os dados do registro grupo de perda <codigo> e <descricao>
    E clico no botao 'Salvar'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'GrupoPerda-InclusaoSIM'
    Entao o sistema deve efetuar a operacao 'GrupoPerda-Inclusao_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu   |codigo|descricao                   |elementotela    |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"28"  |"TESTES GERAIS GRUPO PERDAS"|"Grupo de perda"|

# CONSULTA >>> CENÁRIO QUE EXECUTA A CONSULTA DE GRUPO PERDA
### TESTADO E FUNCIONANDO EM 09/05/2022
@consulta-reg-grupoperda
Cenario: Consultar registro na tabela grupo de perda
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <descod> do registro grupo perda
    Entao o sistema deve exibir o registro grupo perda <descod> consultado 'GrupoPerda-ConsultaReg'

Exemplos:
    |menu       |submenu       |subsubmenu   |descod                   |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"Novo Grupo de Perda TST"|

# CONSULTA DETALHES >>> CENÁRIO QUE EXECUTA A CONSULTA DE DETALHES DE UM GRUPO PERDA
### TESTADO E FUNCIONANDO EM 
@consulta-reg-grupoperda-detas
Cenario: Consultar detalhe do registro na tabela grupo de perda
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <descod> do registro grupo perda
    E clico no botao Detalhes do registro 'GrupoPerda-ConsultarDetalhes' selecionado na matriz <titdetalhetela>
    E clico no botao x para fechar a tela de registro do grupo perda
    Entao o sistema deve efetuar a operacao 'GrupoPerda-ConsultaDetalhe_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu   |descod              |titdetalhetela|elementotela    |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"Edição Grupo Perda"|"Grupo Perda" |"Grupo de perda"|

# INCLUSÃO ABRE E FECHA >>> CENÁRIO QUE ABRE A TELA DE INCLUSÃO E FECHA A MESMA SEM NENHUMA AÇÃO
# A TELA NÃO TEM O BOTÃO "NÃO" E SIM O BOTÃO "x" PARA FECHAR E NÃO GRAVAR
### TESTADO E FUNCIONANDO EM 09/05/2022
@tela-grupo-perda  @inclusao-abre-fecha-tela-gp
Cenario: Acessar a tela de cadastro grupo perda e fechar
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao da tela grupo de perda 'Novo Grupo Perda'
    E clico no botao x para fechar a tela de registro do grupo perda
    Entao o sistema deve efetuar a operacao 'GrupoPerda-IncAbreFechaTela_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu   |codigo|descricao          |elementotela    |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"23"  |"Grp Perda VSJ-TST"|"Grupo de perda"|

# INCLUSÃO DUP >>> CENÁRIO QUE TENTA EFETUAR CADASTRO DE GRUPO DE PERDA JÁ CADASTRADO NA BASE
#                  CLICA NO BOTÃO "SIM" - DUPLICIDADE
### TESTADO E FUNCIONANDO EM 09/05/2022
@inclusao-reg-duplicidade
Cenario: Cadastrar registro na tabela grupo de perda Duplicidade
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao da tela grupo de perda 'Novo Grupo Perda'
    E insiro os dados do registro grupo de perda <codigo> e <descricao>
    E clico no botao 'Salvar'
    E o sistema exibe a mensagem de erro grupo de perda
    E clico no botao 'Fechar' da mensagem de erro grupo de perda 'GrupoPerda-InclusaoDUPLICIDADE'
    E clico no botao x para fechar a tela de registro do grupo perda
    Entao o sistema deve efetuar a operacao 'GrupoPerda-Inclusao-DUP_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu   |codigo|descricao        |elementotela    |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"22"  |"Grupo Perda TST"|"Grupo de perda"|

# EDITAR >>> CENÁRIO QUE EFETUA A EDIÇÃO DE DADOS DE UM GRUPO DE PERDA JÁ EXISTENTE
#            CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM 09/05/2022
@editar-reg-grupoperda
Cenario: Editar registro na tabela grupo perda
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <descod> do registro grupo perda
    E clico no botao na matriz
    E insiro os dados do registro <descricao>
    E clico no botao 'Salvar'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'GrupoPerda-EditarRegSIM'
    Entao o sistema deve efetuar a operacao 'GrupoPerda-EditarReg-DUP_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu   |descod       |descricao           |elementotela    |
    |"Gerencial"|"Tabela perda"|"Grupo perda"|"tsts tststs"|"Edição Grupo Perda"|"Grupo de perda"|

# EXCLUIR NÃO >>> CENÁRIO QUE NÃO EXLCUI UM REGISTRO FONTE ANÁLISE DA BASE
#                 CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 09/05/2022
@nao-excluir-reg-grupoperda
Cenario: Excluir registro Grupo Perda da matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <descod> do registro grupo perda
    E clico no checkbox selecionando o grupo perda
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Não' da mensagem de confirmacao da exclusao 'GrupoPerda-ExcluirNAO'
    Entao o sistema deve efetuar a operacao 'GrupoPerda-ExcluirNAO_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu   |elementotela    |descod                   |
    |"Gerencial" |"Tabela perda"|"Grupo perda"|"Grupo de perda"|"Novo Grupo de Perda TST"|

# EXCLUSÃO >>> CENÁRIO QUE EFETUA A EXCLUSÃO DE UM GRPO DE PERDA EXISTENTE NA BASE
#              CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM 09/05/2022
@excluir-reg-grupoperda
Cenario: Excluir registro Grupo Perda da matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <descod> do registro grupo perda
    E clico no checkbox selecionando o grupo perda
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Sim' da mensagem de confirmacao da exclusao 'GrupoPerda-ExcluirSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'GrupoPerda-ExlcuirFECHAR' de exclusao 
    Entao o sistema deve efetuar a operacao 'GrupoPerda-ExcluirSIM_VOLTA' e retorna para tela matriz grupo de perda <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu   |elementotela    |descod             |
    |"Gerencial" |"Tabela perda"|"Grupo perda"|"Grupo de perda"|"PERDA TSTS VARIOS"|

### CENÁRIO NÃO IMPLEMENTADO - NÃO É VIÁVEL EXCLUIR TODOS OS REGISTROS POR CAUSA DE DEPENDÊNCIAS
    ###@excluir-todos-regs-grupoperda
###Cenario: Excluir todos registros Grupo Perda da matriz
###    E seleciono o <menu> e <submenu> e <subsubmenu>
###    E seleciono todos parametros exibidos <regporpagina> na lista na matriz 
###    E clico no botao 'Excluir' Selecionados
###    E clico no botao 'Sim' da mensagem de confirmacao da exclusao
###    E clico no botao 'Fechar' da mensagem de sucesso da operacao de exclusao
###    Entao o sistema exclui registros selecionados e retorna tela <elementotela>
##### 11/03/2021 - Falta testar esse excluir todos, pois para isso ocorrer, a matriz da tela de Perdas
##### Tem que estar também vazia...
###Exemplos:
###    |menu        |submenu       |subsubmenu   |elementotela    |regporpagina|
###    |"Gerencial" |"Tabela perda"|"Grupo perda"|"Grupo de perda"|"10"        |
