#encoding: utf-8
#language:pt
@0 @login
Funcionalidade: Login no Omni Erp

# LOGIN SUCESSO >>> CENÁRIO QUE EXECUTA O LOGIN COM SUCESSO
@login-sucesso
Cenario: Quando eu realizo o login com sucesso
    Quando eu acesso a pagina do omni erp
    E preencho o nome do <usuario>
    E preencho a <senha>
    E clico no botao Entrar
    Entao valido se consegui acessar o omni erp 'TabelaPerdas-LoginSucesso'

Exemplos:
    |usuario             |senha      |
    |"caroline_cerantola"|"cotuba12"|
    #|"michel_froio"      |"SENHA123" |

# LOGIN FALHA >>> CENÁRIO QUE EXECUTA O LOGIN COM FALHA DE ACESSO
@login-falha
Cenario: Quando eu realizo o login com usuario nao cadastrado
    Quando eu acesso a pagina do omni erp
    E preencho o nome do <usuario>
    E preencho a <senha>
    E clico no botao Entrar
    Entao verifico a <msgretorno> de retorno 'TabelaPerdas-LoginInsucesso'

Exemplos:
    |usuario   |senha      |msgretorno                  |
    |"111TESTE"|"WINTER123"|"Usuário ou senha inválidos."|
    #|"caroline_cerantola"  |"000000"|"Usuário ou senha inválidos."|

