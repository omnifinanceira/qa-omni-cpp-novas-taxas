#encoding: utf-8
#language:pt

@1 @valida-patametros
Funcionalidade: Validar tela de Aprovacao de parametros na Tela Tabela de Perdas

Contexto:
    Dado que eu esteja logado no sistema omni erp
    Quando eu acesso a tela do menu

# APROVA ABRE/FECHA >>> CENARIO QUE ACESSA A TELA DE APROVAÇÃO, MAS CLICA NO
#                       BOTAO "x" PARA FECHAR A TELA SEM EFETUAR APROVAÇÃO
### TESTADO E FUNCIONANDO EM 20/05/2022
@aprova-param-fechatela
Cenario: Acessar tela de aprovacao de parametros e fechar sem aprovar
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    E clico no botao x para fechar a tela 'TabelaPerdas-AprovacaoParamFECHATELA'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-AprovParamAbreFecha_TELA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |aprovtela               |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"Aprovação Tabela perda"|"Tabela de perdas"|

# APROVA NÃO >>> CENÁRIO QUE NÃO CONFRMA A APROVAÇÃO DE PARÂMETRO DA TABELA DE PERDAS
#                CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 20/05/2022
@nao-aprovar-parametros
Cenario: Não Aprovar parametros selecionados para aprovacao
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    E seleciono um <produto> parametro tela de aprovacao
    E clico no checkbox de aprovacao do produto selecionado
    E clico no botao 'Confirmar implantação' dos parametros selecionados
    E clico no botao 'Não' da mensagem de confirmacao da operacao 'TabelaPerdas-AprovacaoNAO'
    E clico no botao x para fechar a tela de aprovacao
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-AprovacaoNAO_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|aprovtela               |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Aprovação Tabela perda"|"Tabela de perdas"|

# APROVAR USUARIO NÃO APROVA >>> CENÁRIO QUE ACESSA A TELA DE APROVAÇÃO COM O MESMO USUÁRIO
#                                QUE INCLUIU OS PARÂMETROS NA TABELA DE PERDAS
### TESTADO E FUNCIONANDO EM 24/05/2022
@usuario-nao-aprova
Cenario: Acessar a tela Aprovacao Tabela perda usuario sem permissao
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    E clico no botao 'Confirmar implantação' e msg 'TabelaPerdas-ConfirmarImplantacaoERRO'
    E clico no botao 'Fechar' da mensagem de erro da tela aprovacao
    E clico no botao 'Negar implantação' e msg 'TabelaPerdas-NegarImplantacaoERRO'
    E clico no botao 'Fechar' da mensagem de erro da tela aprovacao
    E clico no botao x para fechar a tela de aprovacao
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-Aprovacao_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |aprovtela               |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"Aprovação Tabela perda"|"Tabela de perdas"|

# APROVAR PARÂMETROS >>> CENÁRIO QUE EFETUA APROVAÇÃO DE PARÂMETROS DE PERDA COM USUÁRIO
#                        COM USUÁRIO DIFERENTE DO QUE EFETUOU A INCLUSÃO DO MESMO
### TESTADO E FUNCIONANDO EM 24/05/2022
@aprovar-parametro
Cenario: Aprovar parametro de perda selecionado para aprovacao
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    #E seleciono quantidade de registros exibidos <qtdregpagina> na lista na matriz
    E seleciono um <produto> parametro tela de aprovacao
    E clico no checkbox de aprovacao do produto selecionado
    E clico no botao 'Confirmar implantação' dos parametros selecionados
    E clico no botao 'Sim' da mensagem de confirmacao da operacao 'TabelaPerdas-AprovacaoSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'TabelaPerdas-AprovacaoSIM'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-AprovacaoSIM_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|aprovtela               |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Aprovação Tabela perda"|"Tabela de perdas"|

# REPROVAR PARÂMETROS >>> CENÁRIO QUE EFETUA A REPROVAÇÃO DE PARÂMETROS DE PERDA COM
#                         USUÁRIO DIFERENTE DO QUE EFETUOU A INCLUSÃO DO MESMO
### TESTADO E FUNCIONANDO EM: 25/05/2022
@reprovar-parametro
Cenario: Reprovar parametro de perda selecionado
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    E seleciono um <produto> parametro tela de aprovacao
    E clico no checkbox de aprovacao do produto selecionado
    E clico no botao 'Negar implantação' dos parametros selecionados
    E preencho o campo motivo <motivonega> da negacao 'TabelaPerdas-ReprovacaoSIM'
    E clico no botao 'Confirmar' na tela de motivo de negacao
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'TabelaPerdas-ReprovacaoSIM'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-ReprovacaoSIM_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto        |aprovtela               |elementotela      |motivonega             |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE" |"Aprovação Tabela perda"|"Tabela de perdas"|"Negado por Tester VSJ"|

# NÃO APROVAR TODOS >>> CENÁRIO QUE NÃO CONFRMA A APROVAÇÃO DE TODOS PARÂMETRO DA TABELA DE PERDAS
#                       CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 25/05/2022
@nao-aprovar-parametros-todos
Cenario: Nao Aprovar Todos parametros selecionados para aprovacao
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    #E seleciono um <produto> parametro tela de aprovacao
    #E clico no checkbox de aprovacao do produto selecionado
    E clico no botao 'Aprovar todos' dos parametros selecionados
    E clico no botao 'Não' da mensagem de confirmacao da operacao 'TabelaPerdas-AprovacaoTodosNAO'
    E clico no botao x para fechar a tela de aprovacao
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-AprovacaoTodosNAO_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|aprovtela               |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Aprovação Tabela perda"|"Tabela de perdas"|

# APROVAR TODOS >>> CENÁRIO QUE CONFRMA A APROVAÇÃO DE TODOS OS PARÂMETROS DA TABELA DE PERDAS
#                   CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM /05/2022
@aprovar-parametros-todos
Cenario: Aprovar Todos parametros selecionados para aprovacao
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Aprovação' e verifico que a tela aprovacao <aprovtela> foi aberta
    #E seleciono um <produto> parametro tela de aprovacao
    #E clico no checkbox de aprovacao do produto selecionado
    E clico no botao 'Aprovar todos' dos parametros selecionados
    E clico no botao 'Sim' da mensagem de confirmacao da operacao 'TabelaPerdas-AprovacaoTodosSIM'
    E clico no botao x para fechar a tela de aprovacao
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-AprovacaoTodosSIM_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|aprovtela               |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Aprovação Tabela perda"|"Tabela de perdas"|


