#encoding: utf-8
#language:pt

@1 @tela-down-upload
Funcionalidade: Validar funcionamento Tela Tabela de Perdas

Contexto:
    Dado que eu esteja logado no sistema omni erp
    Quando eu acesso a tela do menu

# ACESSAR TELA >>> CENÁRIO QUE TESTA O ACESSO À TELA DO SISTEMA
@acessar-tela
Cenario: Verificar os campos existentes na tela download upload
    E seleciono o <menu> e <submenu> e <subsubmenu>
    Entao eu espero visualizar o elemento <elementotela> na tela acessada  

Exemplos:
    |menu       |submenu       |subsubmenu       |elementotela      |
    |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"Tabela de perdas"|
    #|"Gerencial"|"Tela Download e Upload"|"Tabela perda"|"Verificar elemento"|
 
# IMPORTAR PLANILHA >>> CENÁRIO QUE EXECUTA O FLUXO DE IMPORTAÇÃO DA PLANILHA DE PARÂMETROS
@importar-planilha
Cenario: Acessar a tela para dowload da planilha da Tabela Perdas
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono o arquivo para importacao
    E clico no botao para importar 'Import Excel'
    E clico no botao 'Sim' da mensagem de confirmacao da importacao do arquivo
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'TabelaPerdas-ImportPlanSIM'
    Entao o sistema efetua a importacao dos dados e retorna tela <elementotela>
#
Exemplos:
    |menu       |submenu       |subsubmenu    |elementotela      |
    |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"Tabela de perdas"|
    #|menu       |submenu                      |elementotela      |
    #|"Gerencial"|"Parâmetro Tabelas de Perdas"|"Tabela de perdas"|

# INCLUSÃO >>> CENÁRIO QUE EFETUA O CADASTRO DE PARÂMETROS NA TABELA DE PERDAS 
#              CLICA NO BOTÃO SIM
### TESTADO E FUNCIONANDO EM 05/05/2022
@tela-tab-perda
@incluir-parametros @cad-tela-tabela-perda
Cenario: Cadastrar parametros na tabela perda
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao Nova Tabela Perda
    E insiro os dados do parametro <produto>, <escinfer>, <escsuper>, <prazo>, <entinfer>, <entsuper>, <classagente>, <grupoperda>, <perda> e <fonteanalise>
    E clico no botao 'Salvar'
    E clico no botao 'Sim' da mensagem de confirmacao da inclusao 'TabelaPerda-InclusaoParamSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'TabelaPerda-InclusaoParamSIM_FECHAR'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-InclusaoParam_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
   |menu       |submenu       |subsubmenu       |produto         |escinfer |escsuper |prazo|entinfer |entsuper |classagente |grupoperda                |perda    |fonteanalise|elementotela      |
   # INC 23/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"MOTO"          |"60,0100" |"99,9999"|"6"  |"55,0100"|"60,0000"|"AMARELO"  |"1 - Padrão"              |"5,5000" |"2"         |"Tabela de perdas"|
   # INC 23/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"MOTO"          |"60,0100" |"99,9999"|"12" |"20,0100"|"25,0000"|"AMARELO"  |"1 - Padrão"              |"6,3000" |"2"         |"Tabela de perdas"|
   # INC 24/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"MOTO"          |"60,0100" |"99,9999"|"18" |"0,0000" |"5,0000" |"AMARELO"  |"1 - Padrão"              |"7,1000" |"2"         |"Tabela de perdas"|
   # INC 24/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"MOTO"          |"60,0100" |"99,9999"|"24" |"55,0100"|"60,0000"|"AMARELO"  |"1 - Padrão"              |"5,5000" |"2"         |"Tabela de perdas"|
   # INC 24/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"MOTO"          |"45,0100" |"60,0000"|"48" |"45,0100"|"50,0000"|"AMARELO"  |"1 - Padrão"              |"8,6000" |"2"         |"Tabela de perdas"|
   # INC 24/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"MOTO"          |"45,0100" |"60,0000"|"48" |"50,0100"|"55,0000"|"AMARELO"  |"1 - Padrão"              |"8,5000" |"2"         |"Tabela de perdas"|
   # INC 25/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"60,0100" |"99,9999"|"24" |"20,0100"|"25,0000"|"AMARELO"  |"1 - Padrão"              |"5,3000" |"4"         |"Tabela de perdas"|
   # INC 25/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"60,0100" |"99,9999"|"36" |"70,0100"|"75,0000"|"AMARELO"  |"1 - Padrão"              |"4,3000" |"4"         |"Tabela de perdas"|
   # INC 24/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"60,0100" |"99,9999"|"42" |"50,0100"|"55,0000"|"AMARELO"  |"1 - Padrão"              |"4,7000" |"4"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"60,0100" |"99,9999"|"48" |"40,0100"|"45,0000"|"AMARELO"  |"1 - Padrão"              |"4,8000" |"4"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"0,0000"  |"30,0000"|"6"  |"5,0100" |"10,0000"|"AMARELO"  |"1 - Padrão"              |"34,1200"|"4"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"0,0000"  |"30,0000"|"6"  |"10,0100"|"15,0000"|"AMARELO"  |"1 - Padrão"              |"33,1000"|"4"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"45,0100" |"60,0000"|"18" |"40,0100"|"45,0000"|"AMARELO"  |"1 - Padrão"              |"7,7000" |"4"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"45,0100" |"60,0000"|"18" |"45,0100"|"50,0000"|"AMARELO"  |"1 - Padrão"              |"7,6000" |"4"         |"Tabela de perdas"|
   # INC 25/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"12" |"0,0000" |"5,0000" |"AZUL"     |"4 - Perda Posicionamento"|"7,6000" |"3"         |"Tabela de perdas"|
   # INC 25/05 |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"24" |"0,0000" |"5,0000" |"AZUL"     |"1 - Padrão"              |"25,2000"|"3"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"24" |"0,0000" |"5,0000" |"AZUL"     |"4 - Perda Posicionamento"|"26,6200"|"3"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"30" |"0,0000" |"5,0000" |"AZUL"     |"2 - Perda Zero"          |"25,6700"|"3"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"36" |"0,0000" |"5,0000" |"AZUL"     |"2 - Perda Zero"          |"26,1500"|"3"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"30" |"0,0000" |"5,0000" |"AZUL"     |"4 - Perda Posicionamento"|"25,6700"|"3"         |"Tabela de perdas"|
   ###|"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO PESADO"|"0,0000"  |"31,4900"|"48" |"0,0000" |"5,0000" |"AZUL"     |"2 - Perda Zero"          |"27,1000"|"3"         |"Tabela de perdas"|



# CONSULTA >>> CENÁRIO QUE EXECUTA A CONSULTA DE REGISTROS TABELA DE PERDAS
### TESTADO E FUNCIONANDO EM 05/05/2022
@consultar-parametro @editar
Cenario: Consultar parametros cadastrados na matriz tabela de perdas
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono um <produto> parametro na matriz
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-ConsultarParam_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Tabela de perdas"|

# INCLUSÃO ABRE/FECHA >>> CENARIO QUE ACESSA A TELA PARA INCLUSÃO DE REGISTRO, MAS CLICA NO
#                         BOTAO "x" PARA FECHAR A TELA E NAO PREENCHE OS CAMPOS
### TESTADO E FUNCIONANDO EM 05/05/2022
@cad-param-fechatela
Cenario: Acessar tela de parametros e fechar sem inclusao
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao Nova Tabela Perda
    E clico no botao x para fechar a tela 'TabelaPerdas-InclusaoParamFECHATELA'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-AbreFecha_TELA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu       |elementotela      |
    |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"Tabela de perdas"|

# INCLUSÃO NÃO >>> CENÁRIO QUE NÃO EFETUA O CADASTRO DE NOVO PARÂMETRO NA TABELA DE PERDAS 
#                  CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 05/05/2022
@nao-incluir-parametros
Cenario: Não Cadastrar parametros na tabela perda
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao Nova Tabela Perda
    E insiro os dados do parametro <produto>, <escinfer>, <escsuper>, <prazo>, <entinfer>, <entsuper>, <classagente>, <grupoperda>, <perda> e <fonteanalise>
    E clico no botao 'Salvar'
    E clico no botao 'Não' da mensagem de confirmacao da inclusao 'TabelaPerdas-InclusaoParamNAO'
    E clico no botao x para fechar a tela 'TabelaPerdas-InclusaoParamNAO_FECHAR'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-InclusaoParamNAO_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu       |produto         |escinfer |escsuper |prazo|entinfer |entsuper |classagente|grupoperda  |perda    |fonteanalise|elementotela      |
    |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"  |"49,0000"|"99,9999"|"48" |"95,0100"|"99,9999"|"AMARELO"  |"1 - Padrão"|"3,0000" |"13"        |"Tabela de perdas"|

# INCLUSÃO DUP >>> CENÁRIO QUE TENTA EFETUAR CADASTRO DE PARÂMETRO DE PERDA JÁ CADASTRADO NA BASE
#                  CLICA NO BOTÃO "SIM" - DUPLICIDADE
### TESTADO E FUNCIONANDO EM 05/05/2022
@incluir-param-duplicidade
Cenario: Cadastrar parametros na tabela perda Parametro Duplicidade
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao Nova Tabela Perda
    E insiro os dados do parametro <produto>, <escinfer>, <escsuper>, <prazo>, <entinfer>, <entsuper>, <classagente>, <grupoperda>, <perda> e <fonteanalise>
    E clico no botao 'Salvar'
    E o sistema exibe a tela de duplicidade <duplicidadetela> e 'TabelaPerdas-InclsaoDUP'
    E clico no botao x para fechar a tela 'TabelaPerdas-InclusaoFECHAR-DUP'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-Inclusao-DUP_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu       |produto       |escinfer |escsuper |prazo|entinfer |entsuper |classagente|grupoperda              |perda    |fonteanalise|elementotela      |duplicidadetela       |
    |"Gerencial"|"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"|"0,0000" |"18,0990"|"48" |"90,0100"|"95,0000"|"AMARELO"  |"3 - Perda Cliente Omni"|"35,0500"|"9"         |"Tabela de perdas"|"Confirmar - Conflito"|

# EDITAR >>> CENÁRIO QUE EFETUA A EDIÇÃO DE DADOS DE UM PARÂMETRO DE PERDAS JÁ EXISTENTE
#            CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM 05/05/2022
@editar-parametro @editar
Cenario: Editar parametro cadastrado na matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono um <produto> parametro na matriz
    E clico no botao Editar do registro selecionado na matriz
    E altero alguns dados do parametro <prazo>, <classagente>, <grupoperda> e <perda>
    E clico no botao 'Salvar'
    E clico no botao 'Sim' da mensagem de confirmacao da alteracao
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'TabelaPerdas-EditaRegistroSIM'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-EditaRgistro_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|elementotela      |prazo |classagente|grupoperda  |perda    |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Tabela de perdas"|"36"  |"AMARELO"  |"1 - Padrão"|"15,5000"|

# EDITAR DUP >>> CENÁRIO QUE EFETUA A EDIÇÃO DE DADOS DE UM PARÂMETRO DE PERDAS E USA O MESMO DADO
#                CLICA NO BOTÃO "SIM" - DUPLICIDADE
### TESTADO E FUNCIONANDO EM 05/05/2022
@editar-param-duplicidade @editar
Cenario: Editar parametro que ja foi editado Duplicidade
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono um <produto> parametro na matriz
    E clico no botao Editar do registro selecionado na matriz
    E altero alguns dados do parametro <prazo>, <classagente>, <grupoperda> e <perda>
    E clico no botao 'Salvar'
    E o sistema exibe a tela de duplicidade <duplicidadetela> e 'TabelaPerdas-InclsaoDUP'
    E clico no botao x para fechar a tela 'TabelaPerdas-Editar-DUP'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-Editar-DUP_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto|elementotela      |prazo |classagente|grupoperda  |perda    |duplicidadetela       |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO" |"Tabela de perdas"|"36"  |"AMARELO"  |"1 - Padrão"|"15,5000"|"Confirmar - Conflito"|

# EXCLUSÃO NÃO >>> CENÁRIO QUE NÃO EXLCUI UM PARÂMETRO DA TABELA DE PERDAS
#                  CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 05/05/2022
@nao-excluir-parametro
Cenario: Não Excluir parametros cadastrados na matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono um <produto> parametro na matriz
    E clico no checkbox selecionando o produto
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Não' da mensagem de confirmacao da exclusao 'TabelaPerdas-ExcluirNAO'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-ExcluirNAO_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"MOTO"  |"Tabela de perdas"|

# EXCLUIR >>> CENÁRIO QUE EXCLUI UM PARÂMETRO DA TABELA DE PERDAS
#             CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM 05/05/2022
@excluir
@excluir-parametro
Cenario: Excluir parametros cadastrados na matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono um <produto> parametro na matriz
    E clico no checkbox selecionando o produto
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Sim' da mensagem de confirmacao da exclusao 'TabelaPerdas-ExcluirSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao de exclusao 'TabelaPerdas-Excluir_FECHAR'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-Excluir_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |produto       |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"VEÍCULO LEVE"|"Tabela de perdas"|

# EXCLUIR TODOS DA TELA >>> CENÁRIO QUE EFETUA A EXCLUSÃO DE TODOS OS REGISTROS SELECIONADOS
### TESTADO E FUNCIONANDO EM 06/05/2022
@excluir-todos-parametros
Cenario: Excluir todos os parametros cadastrados na matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E seleciono todos parametros exibidos <regporpagina> na lista na matriz 
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Sim' da mensagem de confirmacao da exclusao 'TabelaPerdas-ExcluirTodosSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao de exclusao 'TabelaPerdas-ExcluirTodos_FECHAR'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-ExcluirTodos_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |elementotela      |regporpagina|
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"Tabela de perdas"|"10"        |

# EXPORTAR >>> CENÁRIO QUE EFETUA A EXPORTAÇÃO DAS PERDAS PARA PLANILHA EXCEL
@exportar-excel
Cenario: Acessar a tela e exportar dados para planilha excel
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao 'Export Excel'
    Entao o sistema deve efetuar a operacao 'TabelaPerdas-ExportExcel_VOLTA' e retorna para tela matriz tabela de perdas <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu       |elementotela      |
    |"Gerencial" |"Tabela perda"|"Tabela de Perda"|"Tabela de perdas"|