#encoding: utf-8
#language:pt

@3 @tela-fonteanalise
Funcionalidade: Validar funcionamento Tela Fonte Analise

Contexto:
    Dado que eu esteja logado no sistema omni erp
    Quando eu acesso a tela do menu

# ACESSAR TELA FONTE ANALISE - EFETUA TESTE DE ACESSO A TELA
### TESTADO E FUNCIONANDO EM 02/05/2022
@acessar-tela-fonteanalise
Cenario: Verificar os campos existentes na tela download upload
    E seleciono o <menu> e <submenu> e <subsubmenu>
    Entao eu espero visualizar o elemento <elementotela> na tela fonte analise  

Exemplos:
    |menu       |submenu       |subsubmenu     |elementotela   |
    |"Gerencial"|"Tabela perda"|"Fonte análise"|"Fonte análise"|
 
# INCLUSÃO >>> CENÁRIO QUE EFETUA O CADASTRO DE NOVA FONTE ANÁLISE 
#              CLICA NO BOTÃO SIM
### TESTADO E FUNCIONANDO EM 02/05/2022
@tela-fonte-analise  @cad-registros-fonteanalise
Cenario: Cadastrar registros na tabela fonte analise
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao da tela fonte analise 'Nova Fonte Análise'
    E insiro os dados do registro fonte analise <descricao> e <chave>
    E clico no botao 'Salvar'
    E clico no botao 'Sim' para confirmar inclusao fonte analise 'FonteAnalise-CadRegistroSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao fonte analise
    Entao o sistema deve efetuar a operacao 'FonteAnalise-CadRegistroSIM' e retorna para tela matriz fonte analise <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu     |descricao      |chave          |elementotela   |
    |"Gerencial"|"Tabela perda"|"Fonte análise"|"NEW VSJ 01-05"|"NEW_VSJ_01_05"|"Fonte análise"|

# CONSULTA >>> CENÁRIO QUE EXECUTA A CONSULTA DE FONTE ANÁLISE
### TESTADO E FUNCIONANDO EM 02/05/2022
@consulta-reg-fonteanalise
Cenario: Consultar registro na tabela fonte analise
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <chavecod> do registro fonte analise
    Entao o sistema deve efetuar exibir o registro consultado <chavecod>

Exemplos:
    |menu       |submenu       |subsubmenu     |chavecod             |
    |"Gerencial"|"Tabela perda"|"Fonte análise"|"NEW_FTE_ANALISE_TST"|

# INCLUSÃO NÃO >>> CENÁRIO QUE NÃO EFETUA O CADASTRO DE NOVA FONTE ANÁLISE
#          CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 02/05/2022
@nao-cadastrar-reg-fonteanalise
Cenario: Nao cadastrar registros na tabela fonte analise
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao da tela fonte analise 'Nova Fonte Análise'
    E insiro os dados do registro fonte analise <descricao> e <chave>
    E clico no botao 'Salvar'
    E clico no botao 'Não' para confirmar inclusao fonte analise 'FonteAnalise-CadRegistroNAO'
    E clico no botao x para fechar a tela de registro da fonte analise
    Entao o sistema deve efetuar a operacao 'FonteAnalise-CadRegistroNAO' e retorna para tela matriz fonte analise <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu     |descricao            |chave                 |elementotela   |
    |"Gerencial"|"Tabela perda"|"Fonte análise"|"INC TST FTE ANALISE"|"INC_TST_FTE_ANALISEA"|"Fonte análise"|

# INCLUSÃO DUP >>> CENÁRIO QUE TENTA EFETUAR CADASTRO DE FONTE ANÁLISE JÁ CADASTRADA NA BASE
#                  CLICA NO BOTÃO "SIM" - DUPLICIDADE
### TESTADO E FUNCIONANDO EM 02/05/2022
@cad-reg-dupli-fonteanalise @cad-fonte-analise-dupli
Cenario: Cadastrar registro na tabela fonte analise Duplicidade
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E clico no botao da tela fonte analise 'Nova Fonte Análise'
    E insiro os dados do registro fonte analise <descricao> e <chave>
    E clico no botao 'Salvar'
    E clico no botao 'Sim' para confirmar inclusao fonte analise 'FonteAnalise-CadRegistroDUPLICIDADE'
    E o sistema exibe a mensagem de erro fonte analise 'FonteAnalise-CadRegistroDUPLICIDADE'
    E clico no botao 'Fechar' da mensagem de erro
    E clico no botao x para fechar a tela de registro da fonte analise
    Entao o sistema deve efetuar a operacao 'FonteAnalise-CadRegistroDUPLICIDADE' e retorna para tela matriz fonte analise <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu     |descricao            |chave                 |elementotela   |
    |"Gerencial"|"Tabela perda"|"Fonte análise"|"INC TST FTE ANALISE"|"INC_TST_FTE_ANALISEA"|"Fonte análise"|

# EDITAR >>> CENÁRIO QUE EFETUA EDIÇÃO DE DADOS DE FONTE ANÁLISE JÁ EXISTENTE
#            CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM 02/05/2022
@editar-reg-fonteanalise
Cenario: Editar registro na tabela fonte analise
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <chavecod> do registro fonte analise
    E clico no botao Editar do registro selecionado na matriz
    E altero os dados do registro fonte analise <descricao> e <chave>
    E clico no botao 'Salvar'
    E clico no botao 'Sim' para confirmar alteracao de fonte analise 'FonteAnalise-EditaRegistroSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao 'FonteAnalise-EditaRegistroSIM'
    Entao o sistema deve efetuar a operacao 'FonteAnalise-EditaRegistroSIM' e retorna para tela matriz fonte analise <elementotela>

Exemplos:
    |menu       |submenu       |subsubmenu     |chavecod            |descricao           |chave               |elementotela   |
    |"Gerencial"|"Tabela perda"|"Fonte análise"|"FTE_ANALISE_TST-02"|"FTE ANALISE TST-03"|"FTE_ANALISE_TST-03"|"Fonte análise"|

# EXCLUSÃO NÃO >>> CENÁRIO QUE NÃO EXLCUI UMA FONTE ANÁLISE DA BASE
#              CLICA NO BOTÃO "NÃO"
### TESTADO E FUNCIONANDO EM 02/05/2022
@nao-excluir-reg-fonteanalise
Cenario: Nao excluir registro Fonte Analise da matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <chavecod> do registro fonte analise
    E clico no checkbox selecionando a fonte analise
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Não' da mensagem de confirmacao da exclusao fonte analise 'FonteAnalise-ExlcuirNAO'
    Entao o sistema deve efetuar a operacao 'FonteAnalise-ExlcuirNAO' e retorna para tela matriz fonte analise <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu     |elementotela   |chavecod              |
    |"Gerencial" |"Tabela perda"|"Fonte análise"|"Fonte análise"|"INC_TST_FTE_ANALISEA"|

# EXCLUSÃO >>> CENÁRIO QUE EFETUA A EXCLUSÃO DE FONTE ANÁLISE EXISTENTE NA BASE
#              CLICA NO BOTÃO "SIM"
### TESTADO E FUNCIONANDO EM 02/05/2022
@excluir-reg-fonteanalise
Cenario: Excluir registro Fonte Analise da matriz
    E seleciono o <menu> e <submenu> e <subsubmenu>
    E insiro o <chavecod> do registro fonte analise
    E clico no checkbox selecionando a fonte analise
    E clico no botao 'Excluir' Selecionados
    E clico no botao 'Sim' da mensagem de confirmacao da exclusao fonte analise 'FonteAnalise-ExlcuirSIM'
    E clico no botao 'Fechar' da mensagem de sucesso da operacao de exclusao
    Entao o sistema deve efetuar a operacao 'FonteAnalise-ExlcuirSIM' e retorna para tela matriz fonte analise <elementotela>

Exemplos:
    |menu        |submenu       |subsubmenu     |elementotela   |chavecod             |
    |"Gerencial" |"Tabela perda"|"Fonte análise"|"Fonte análise"|"INC_TST_FTE_ANALISE"|

# ESTE CENÁRIO AINDA NÃO FOI DESENVOLVIDO
####@excluir-todos-regs-fonteanalise
####Cenario: Excluir todos registros Fonte Analise da matriz
####    E seleciono o <menu> e <submenu> e <subsubmenu>
####    E seleciono todos parametros exibidos <regporpagina> na lista na matriz 
####    E clico no botao 'Excluir' Selecionados
####    E clico no botao 'Sim' da mensagem de confirmacao da exclusao
####    E clico no botao 'Fechar' da mensagem de sucesso da operacao de exclusao
####    #Entao o sistema exclui registros selecionados e retorna tela <elementotela>
####    Entao o sistema deve efetuar a operacao e retorna para tela matriz fonte analise <elementotela>
###### 22/04/2021 - Este cenário não foi testado poir para excluir todos os registros da matriz 
###### não pode haver nenhum registor de fonte análise ligada em nenhum registro de perda
####Exemplos:
####    |menu        |submenu       |subsubmenu     |elementotela   |regporpagina|
####    |"Gerencial" |"Tabela perda"|"Fonte análise"|"Fonte análise"|"10"        |

