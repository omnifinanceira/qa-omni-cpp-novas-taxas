#encoding: utf-8

Quando("eu acesso a tela do menu") do
    ##home = Home.new
    #home.acessar_novo_menu
    @metodoserp = MetodosErp.new
    @util = Util.new
end

Quando('seleciono o {string} e {string} e {string}') do |menu, submenu, subsubmenu|
    @metodoserp.selecionar_menu(menu)
    @metodoserp.selecionar_submenu(submenu)
    @metodoserp.selecionar_subsubmenu(subsubmenu)
end

Entao('eu espero visualizar o elemento {string} na tela acessada') do |elementotela|
    #expect(page.has_xpath?( "//*[@id='single-spa-application:omni-limite-global-mf']/omni-limite-global-mf/section/div/app-oferta-limite-global-lista/div[1]/h4[text()='#{elementotela}']")).to eq true
    find('h3', text: "#{elementotela}", wait:5).visible?
end

#find('a', text: "#{submenu}")

Quando('seleciono o arquivo para importacao') do
    ### Por sugestão do Gabriel foram regitados todos os == false doa whiles.
    ### Se der erro de loop infinito, voltar o false para os whiles.
    @metodoserp.aguardar_carregando
    find('a', text:"Nova Tabela Perda", wait:20).visible?
    page.attach_file('C:/WorkspaceVsj/qa-omni-cpp-novas-taxas/20220308 Importação-2.xlsx') do
        page.find("input[type='file']").click
    end
end

Quando('clico no botao para importar {string}') do |btnome|
    find('a', text:"#{btnome}").click
end

Entao('o sistema efetua a importacao dos dados e retorna tela {string}') do |elementotela|
    find('h3', text: "#{elementotela}").visible?
    ##expect(page.has_xpath?( "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[1]/div/h3[text()='#{elementotela}']")).to eq true
    sleep 40
end

Quando('clico no botao Nova Tabela Perda') do
    find('a', text:"Nova Tabela Perda", wait:15).visible?
    #sleep 5
    @metodoserp.clicar_nova_tabela_perda
end

Quando('insiro os dados do parametro {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string} e {string}') do |produto, escinfer, escsuper, prazo, entinfer, entsuper, classagente, grupoperda, perda, fonteanalise|
    @metodoserp.preencher_parametros(produto, escinfer, escsuper, prazo, entinfer, entsuper, classagente, grupoperda, perda, fonteanalise)
end

# CLICA NO BOTÃO SALVAR PARA INCLUSÃO DE REGISTRO NAS MATRIZES
# UTILIZADO NAS TRES: PARÂMETROS PERDAS, GRUPO DE PERDA E FONTE ANÁLISE
Quando('clico no botao {string}') do |btnome|
    #@metodoserp.clicar_salvar('Salvar')
    #@metodoserp.salvar_registro('Salvar')
    click_on(btnome)
end

# Clica no botão Sim de confirmação da importação do Arquivo Excel
Quando('clico no botao {string} da mensagem de confirmacao da importacao do arquivo') do |btnome|
    click_on(btnome)
end

# CLICA NO BOTÃO SIM NA TELA DA MENSAGEM DE CONFIRMAÇÃO DE SALVAR OU NÃO OS DADOS DA TELA
Quando('clico no botao {string} da mensagem de confirmacao da inclusao {string}') do |btnome, opertp|
    find('p', text:"Deseja criar esta informação?", wait:10)
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# CLICA NO BOTÃO FECHAR DA MENSAGEM DE SUCESSO DA OPERAÇÃO
# UTILIZADO POR TABELA DE PERDA E GRUPO PERDA
Quando('clico no botao {string} da mensagem de sucesso da operacao {string}') do |btnome, opertp|
    msgsucesso = find(:xpath, "//div/div/p").text
    find('p', text: msgsucesso, wait:10).visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

Entao('o sistema deve cadastrar o parametro na matriz e retorna tela {string}') do |elementotela|
    expect(page.has_xpath?( "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[1]/div/h3[text()='#{elementotela}']")).to eq true
    sleep 3
    ##tirar_foto_allure("1d_IncluirParametro-TelaInicial_Sucesso", "TESTADO")
end

### EN 03/05/2022 RETIRADO ESSE CÓDIGO E SUBSTITUIDO PELO ABAIXO, UNIFICANDO PARA TODOS OS CENARIOS
### DESTE TESTE
###Entao('o sistema deve fechar a tela e retornar para tela matriz {string}') do |elementotela|
###    find('a', text:"Nova Tabela Perda", wait:15).visible?
###end

# CÓDIGO QUE APÓS EFETUAR A OPERAÇÃO RETORNA PARA A TELA DA MATRIZ DA TABELA DE PERDAS
Entao('o sistema deve efetuar a operacao {string} e retorna para tela matriz tabela de perdas {string}') do |opertp, elementotela|
    find('h3', text: "#{elementotela}", wait:5).visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    # REFATORAR ESTE ENTÃO PARA TODOS OS CENÁRIOS DE telas-novas-taxas-perdas.feature
end

Quando('o sistema exibe a tela de duplicidade {string} e {string}') do |duplicidadetela, opertp|
    find('h5', text:"Confirmar - Conflito", wait:10).visible? == true
    tirar_foto_allure("PrjTabPerdas-#{opertp}", "TESTADO")
    find("button[class='btn']").click
end

### EM 03/05/2022 FOI RETORADO E UNIFICADO PELO CÓDIGO ABAIXO PARA AS TRES TELAS
###Quando('clico no botao x para fechar a tela parametro') do
###    find("button[class='close']").click
###    #@metodoserp.btn_x_fechar_tela
###end

# CLICA NO BOTÃO "x" PARA FECHAR A TELA.
# UTILIZADO NAS 3 TELAS: TABELA DE PERDAS | GRUPO DE PERDAS | FONTE ANÁLISE
Quando('clico no botao x para fechar a tela {string}') do |opertp|
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    find("button[class='close']").click
end

Entao('o sistema nao cadastra parametro e retorna tela {string}') do |elementotela|
    ###telaprincipal = find()
    expect(page.has_xpath?( "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[1]/div/h3[text()='#{elementotela}']")).to eq true
    ##tirar_foto_allure("Retorna_TelaInicial", "TESTADO")
    sleep 3
end

# Clica no checkbox que seleicona o primeiro regstro da seleção do produto
Quando('clico no checkbox selecionando o produto') do 
    @metodoserp.clicar_checkbox
    ## Não funcionou. Acha o elemento mas não clica. Verificar
    ##first("input[class='ng-untouched ng-pristine ng-valid']").click
end

# Clica no botão Excluir registros selecionados
Quando('clico no botao {string} Selecionados') do |btnome|
    click_on(btnome)
end

# CLICA NO BOTÃO SIM NA MENSAGEM DE CONFIRMAÇÃO DA EXCLUSÃO DE REGISTRO - PARÂMETRO OU PERDA
Quando('clico no botao {string} da mensagem de confirmacao da exclusao {string}') do |btnome, opertp|
    find('h5', text:"Apagar registro", wait:5).visible?
    ##tirar_foto_allure("ExcluirParametro-Botao_SIM", "TESTADO")
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# Clica no botão Fechar da mensagem de sucesso da operação
Quando('clico no botao {string} da mensagem de sucesso da operacao {string} de exclusao') do |btnome, opertp|
    #@metodoserp.clicar_salvar_sim_fechar
    #@metodoserp.salvar_registro('Fechar')
    find('p', text:"Registro(s) excluído(s) com sucesso", wait:15).visible?
    ##tirar_foto_allure("ExcluirParametro-Botao_FECHAR", "TESTADO")
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# Clica no checkbox que seleciona todos os registros da matriz
##Quando('seleciono todos parametros exibidos {string} da lista na matriz') do |regporpagina|
##    @metodoserp.selecionar_todos_registros(regporpagina)
##end

Quando('seleciono todos parametros exibidos {string} na lista na matriz') do |regporpagina|
    @metodoserp.selecionar_todos_registros(regporpagina)
end

# Retorna para a tela da matriz após a exclusão dos registros
Entao('o sistema exclui registros selecionados e retorna tela {string}') do |elementotela|
    expect(page.has_xpath?( "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[1]/div/h3[text()='#{elementotela}']")).to eq true
    sleep 3
    ##tirar_foto_allure("ExcluirParametro-VoltaTelaInicial", "TESTADO")
end

Quando('clico no botao {string} da mensagem de sucesso da operacao de exclusao {string}') do |btnome, opertp|
    find('p', text:"Registro(s) excluído(s) com sucesso", wait:10)
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# Seleciono um produto na matriz através do campo Busca
Quando('seleciono um {string} parametro na matriz') do |produto|
    find('a', text:"Nova Tabela Perda", wait:15).visible?
    find(:xpath, "//input[@placeholder='Buscar']").click
    find(:xpath, "//input[@placeholder='Buscar']").set("#{produto}")
    ##tirar_foto_allure("3_EditarParametro-Selcionar", "TESTADO")
end

# RETIRADO EM 13/05/2022 POIS ESTAVA CONFLITANDO COM OUTRO CÓDIGO 
###Quando('seleciono um {string} da lista na matriz') do |produto|
###    find(:xpath, "//input[@placeholder='Buscar']").click
###    find(:xpath, "//input[@placeholder='Buscar']").set("#{produto}")
###    ##tirar_foto_allure("5a_ExcluirParametro-Selecionar", "TESTADO")
###end

# CLICA NO BOTÃO EDITAR DO PRIMEIRO REGISTRO DA SELEÇÃO DE REGISTROS ATRAVÉS DO FILTRO
#   PASSO UTILIZADO PELAS TRES TELAS TABELA DE PERDAS, GRUPO DE PERDA, FONTE ANÁLISE
Quando('clico no botao Editar do registro selecionado na matriz') do
    first("button[title='Editar']").click
    msgsucesso = find(:xpath, "//div/div/h5").text
    ###find('h4', text:"Políticas de crédito", wait:10).visible?
    find('h5', text: msgsucesso, wait:10).visible?
end

# CLICA NO BOTÃO VER DETALHES DO REGISTRO
Quando('clico no botao Detalhes do registro {string} selecionado na matriz {string}') do |opertp, titdetalhetela|
    #binding.pry
    first("button[title='Detalhes']").click
    find('h5', text: titdetalhetela, wait:10).visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    ##msgsucesso = find(:xpath, "//div/div/h5").text
    ##find('h5', text: msgsucesso, wait:10).visible?
end

#Quando('clico no botao Detalhes do registro selecionado na matriz') do
#    pending # Write code here that turns the phrase above into concrete actions
#end

# Efetuo a alteração de alguns campos
Quando('altero alguns dados do parametro {string}, {string}, {string} e {string}' ) do |prazo, classagente, grupoperda, perda|
    @metodoserp.alterar_dados_parametro(prazo, classagente, grupoperda, perda)
end

# Clica no botão Sim na mensagem de confirmação de Salvar os dados
Quando('clico no botao {string} da mensagem de confirmacao da alteracao') do |btnome|
    #@metodoserp.clicar_salvar_sim
    #@metodoserp.salvar_registro('Sim')
    find('p', text:"Deseja alterar esta informação?", wait:10).visible?
    ##tirar_foto_allure("3a_EditarParametro-Botao_SIM", "TESTADO")
    click_on(btnome)
end

### OBS.: A mensagem do botão Fechar é a mesma para Inlcusão e para Edição de registro

# Sistema retorna para a tela da Matriz após salvar a edição dos dados
Entao('o sistema salva os dados alterados e retorna tela {string}') do |elementotela|
    expect(page.has_xpath?( "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[1]/div/h3[text()='#{elementotela}']")).to eq true
    sleep 3
end

### código para comparar o texto da mensagem de erro.
###### código para comparar o texto da mensagem de erro.texto = find("center").text
###  expect(texto.include?("Dados enviados com sucesso!")).to eq true