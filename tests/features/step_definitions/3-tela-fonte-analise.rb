#encoding: utf-8

Entao('eu espero visualizar o elemento {string} na tela fonte analise') do |elementotela|
    find('h3', text: "#{elementotela}").visible?
end

# CLICA NO BOTÃO PARA ABRIR A TELA DE INCLUSÃO DE NOVO REGISTRO DE FONTE ANÁLISE
Quando('clico no botao da tela fonte analise {string}') do |btnome|
    sleep 5
    #page.assert_text('Nova Fonte Análise')
    #expect(page.assert_text('Nova Fonte Análise')).to eq true
    find('a', text:"Nova Fonte Análise", wait:30)
    click_on(btnome)
end

# CHAMA MÉTODO QUE INCLUI OS DADOS DO FONTE ANÁLISE, PASSANDO AS VARIÁVEIS DOS CAMPOS
Quando('insiro os dados do registro fonte analise {string} e {string}') do |descricao, chave|
    @metodoserp.preencher_fonte_analise(descricao, chave)
end

# CHAMA O MÉTODO QUE CLICA NO CHECK BOX DO REGISTRO FONTE ANÁLISE SELECIONADO PARA EXCLUSÃO
Quando('clico no checkbox selecionando a fonte analise') do
    @metodoserp.clicar_checkbox_fa
end

# CÓDIO ABAIXO USADO PARA CLICAR NO BOTÃO "SIM" E TAMBÉM NO BOTÃO "NÃO" DA INCLUSÃO DE FONTE ANÁLISE
# CLICA NOS BOTÕES "SIM"/"NÃO" DA TELA DE CONFIRMAÇÃO DE INCLUSÃO DA FONTE ANÁLISE
Quando('clico no botao {string} para confirmar inclusao fonte analise {string}') do |btnome, opertp|
    find('p', text:"Deseja inserir esta Fonte Análise?", wait:20)
    #Tirar_foto_1("C:/results/screenshots","1b_IncluirRegistroFonteAnalise-Botao_SIM")
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# CLICA NO BOTÃO "FECHAR" DA MENSAGEM DE SUCESSO DA INCLUSÃO DA FONTE ANÁLISE
Quando('clico no botao {string} da mensagem de sucesso da operacao fonte analise') do |btnome|
    find('p', text:"Fonte Análise salva com sucesso!", wait:20)
    #tirar_foto("RetornaTela-Inicial-FonteAnalise", "TESTADO")
    ##tirar_foto_1("RetornaTela-Inicial-FonteAnalise","TESTADO")
    click_on(btnome)
end

# RETORNA PARA A TELA INICIAL DA MATRIZ DA FONTE ANÁLISE, APÓS CONSLUSÃO DE OPERAÇÕES
Entao('o sistema deve efetuar a operacao {string} e retorna para tela matriz fonte analise {string}') do |opertp, elementotela|
    find('h3', text: "#{elementotela}", wait:5).visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
end

# EXIBE A MENSAGEM DE ERRO DE DUPLICIDADE DE REIGSTRO DE FONTE ANÁLISE
Quando('o sistema exibe a mensagem de erro fonte analise {string}') do |opertp|
    msgerrodupli = find(:xpath, "//div/div/h5[contains(text(),'Falha ao atualizar Fontes Análise!')]").text
    #msgerrodupli = find('h5').text
    find(:xpath, "//div/div/h5[contains(text(),'Falha ao atualizar Fontes Análise!')]", text: "#{msgerrodupli}").visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
end

### EM 03/05/2022 RETIRADO ESSE CÓDIGO E UNIFACADO PELO CÓDIGO NA FEATURE DO TELAS-NOVAS-TAXAS-PERDAS
# CLICA NO BOTÃO "x" PARA FECHAR A TELA APÓS FECHAR A MENSAGEM DE ERRO DE DUPLICIDADE
###Quando('clico no botao x para fechar a tela de registro da fonte analise') do
###    find("button[class='close']").click
###end

# UTILIZA A CHAVE PARA EFETUAR A BUSCA NA MATRIZ FONTE ANÁLISE
Quando('insiro o {string} do registro fonte analise') do |chavecod|
    sleep 2
    #@metodoserp.consulta_codigo_fonte_analise(codigo)
    # Este código localizar o elemento pelo campo onde é digitado o texto para busca
    #find("input[placeholder='Descrição']").set(descod)
    # Este código localiza o título da coluna, utilizando a classe para localizar o elemento
    #find('div.div-sort-icon-label', text:'Chave').set(chavecod)
    find("input[placeholder='Chave']").set(chavecod)
end
 
# CLICA NO BOTÃO SIM DA MENSAGEM DE CONFIRMAÇÃO DA ALTERAÇÃO DE DADOS DA FONTE ANÁLISE
Quando('clico no botao {string} para confirmar alteracao de fonte analise {string}') do |btnome, opertp|
    find('p', text:"Deseja alterar esta Fonte Análise?", wait:10)
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# CLICA NO BOTÃO SIM OU NÃO DA MENSAGEM DE CONFIRMAÇÃO DE EXCLUSÃO DE FONTE ANÁLISE DA BASE
Quando('clico no botao {string} da mensagem de confirmacao da exclusao fonte analise {string}') do |btnome, opertp|
    #find('p', text:"[contains(text(),'Deseja excluir fonte análise chave')]").text
    find(:xpath, "//div/div/p[contains(text(),'Deseja excluir fonte')]").visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

Quando('altero os dados do registro fonte analise {string} e {string}') do |descricao, chave|
    @metodoserp.altera_dados_fonte_analise(descricao, chave)
end

Entao('o sistema deve efetuar exibir o registro consultado {string}') do |chavecod|
    conteudo = find(".searchTerm", text:"#{chavecod}").text
    expect(conteudo).to eq chavecod
    tirar_foto_allure("PrjTabPerdas-Consulta de Registro","TESTADO")
end

###Entao('o sistema exclui registros fonte analise selecionados e retorna tela {string}') do |elementotela|
###    find('h3', text: "#{elementotela}", wait:7).visible?
###end

###Entao('o sistema salva os dados alterados da fonte analise e retorna tela {string}') do |elementotela|
###    find('h3', text: "#{elementotela}", wait:7).visible?
###end
