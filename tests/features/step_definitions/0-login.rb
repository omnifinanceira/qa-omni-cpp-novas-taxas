#encoding: utf-8

Quando('eu acesso a pagina do omni erp') do
  @login = Login.new
  @login.load
end

Quando('preencho o nome do {string}') do |usuario|
  @login.preencher_usuario(usuario)
end

Quando('preencho a {string}') do |senha|
  @login.preencher_senha(senha)
end

Quando('clico no botao Entrar') do
  @login.clicar_entrar
end

Entao('valido se consegui acessar o omni erp {string}') do |opertp|
  sleep 5
  expect(page.has_xpath?( "//*[@id='single-spa-application:omni-login-mf']/omni-login-mf/menu-page/header/nav/a/img")).to eq true
  tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
end

Entao('verifico a {string} de retorno {string}') do |msgretorno, opertp|
  #textofalha = find("div[class='alert alert-danger']")
  find(".alert", text: "#{msgretorno}")
  tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
  #textofalha = find(:xpath, "//div[@class='alert alert-danger']")
  #expect(find(:xpath, "//*[@id='login-container']/div/form/div[1]][text()='#{msgretorno}']")).to be_truthy
  #texto = find("//*[@id='login-container']/div/form/div[1]").text
  #expect(find(:xpath, "//*[@id='login-container']/div/form/div[1]][text()='Usuario ou senha invalidos.']").visible?).to be_truthy
  #texto = find(:xpath, "//*[@id='login-container']/div/form/div[1]").text
  #expect(texto.include?(" #{msgretorno} ")).to eq true

  #find('class', text: "#{submenu}").click
  #find("select[class='alert alert-danger']")
  #find('div', text: "#{msgretorno}")
end

# compara conteúdos:  msgretorno.eql? find(:xpath, "//div[@class='alert alert-danger']").text
