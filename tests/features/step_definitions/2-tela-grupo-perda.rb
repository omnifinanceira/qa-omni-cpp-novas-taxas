#encoding: utf-8

Entao('eu espero visualizar o elemento {string} na tela grupo perda') do |elementotela|
    find('h3', text: "#{elementotela}").visible?
end

Quando('clico no botao da tela grupo de perda {string}') do |btnome|
    sleep 5
    find('a', text:"Novo Grupo Perda", wait:30)
    ###@metodoserp.clicar_nova_tabela_perda(eletipo = "a", eletexto = "Novo Grupo Perda")
    click_on(btnome)
end
  
Quando('insiro os dados do registro grupo de perda {string} e {string}') do |codigo, descricao|
    @metodoserp.preencher_grupo_perda(codigo, descricao)
end

### EM 04/05/2022 CRIADO ESSE ENTÃO PARA SER ÚNICO E ATENDER TODOS OS CENÁRIOS DE GRUPO PERDA
Entao('o sistema deve efetuar a operacao {string} e retorna para tela matriz grupo de perda {string}') do |opertp, elementotela|
    find('h3', text: "#{elementotela}", wait:5).visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
end

### EM 04/05/2022 SUBSTITUIR ESSE ENTÃO POR UM ÚNICO QUE ATENDERÁ TODOS OS CENÁRIOS DE GRUPO PERDA
###Entao('o sistema deve cadastrar o grupo perda na matriz e retorna tela {string}') do |elementotela|
###    find('h3', text: "#{elementotela}").visible?
###end

# CONFIRMA O REGISTRO DE GRUPO PERDA QUE FOI CONSULTADO NO TESTE COM O DA TELA
Entao('o sistema deve exibir o registro grupo perda {string} consultado {string}') do |descod, opertp|
    conteudo = find(".searchTerm", text:"#{descod}").text
    expect(conteudo).to eq descod
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
end

Quando('o sistema exibe a mensagem de erro grupo de perda') do
    msgerro = find('p').text
    #msgerro = find(:xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-grupo-perda-list/omni-modal/div[2]/div/div/div[2]/app-grupo-perda/omni-dialog/omni-modal/div[2]/div/div/div[2]/p")
    find('p', text: "#{msgerro}").visible?
end

# CLICA NO BOTÃO FECHAR DA MENSAGEM DE ERRO DE DUPLICIDADE DE GRUPO DE PERDA
Quando('clico no botao {string} da mensagem de erro grupo de perda {string}') do |btnome, opertp|
    #find('p', text:"Fonte Análise salva com sucesso!", wait:20)
    #msgsucesso = find('p', text:"Tabela Perda salva com sucesso!").text
    msgsucesso = find(:xpath, "//div/div/p").text
    find('p', text: msgsucesso, wait:10).visible?
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    click_on(btnome)
end

# CLICA NO BOTÃO "x" DA TELA PARA FECHAR A MESMA
Quando('clico no botao x para fechar a tela de registro do grupo perda') do
    find("button[class='close']").click
end

### EM 04/05/2022 SUBSTIRUIR ESSE ENTÃO POR UM ÚNICO QUE ATENDERÁ TODOS OS CENÁRIOS DE GRUPO PERDA
###Entao('o sistema nao cadastra registro grupo de perda e retorna tela {string}') do |elementotela|
###    find('h3', text: "#{elementotela}").visible? 
###end

# UTILIZA A CHAVE PARA EFETUAR A BUSCA NA MATRIZ FONTE ANÁLISE
Quando('insiro o {string} do registro grupo perda') do |descod|
    sleep 2
    find("input[placeholder='Descrição']").set(descod)
end

Quando('clico no checkbox selecionando o grupo perda') do
    @metodoserp.clicar_checkbox_gp
end

### EM 04/05/2022 SUBSTIRUIR ESSE ENTÃO POR UM ÚNICO QUE ATENDERÁ TODOS OS CENÁRIOS DE GRUPO PERDA
###Entao('o sistema exclui registros grupo perda selecionados e retorna tela {string}') do |elementotela|
###    find('h3', text: "#{elementotela}").visible?
###end

###Quando('insiro o {string} do registro grupo perda') do |codigo|
###    @metodoserp.consulta_codigo_grupo_perda(codigo)
###end

Quando('insiro os dados do registro {string}') do |descricao|
    @metodoserp.altera_dados_grupo_perda(descricao)
end

### EM 04/05/2022 SUBSTIRUIR ESSE ENTÃO POR UM ÚNICO QUE ATENDERÁ TODOS OS CENÁRIOS DE GRUPO PERDA
###Entao('o sistema salva os dados alterados o grupo perda na matriz e retorna tela {string}') do |elementotela|
###    find('h3', text: "#{elementotela}").visible?
###end