#encoding: utf-8

##Quando('seleciono quantidade de registros exibidos {string} na lista na matriz') do |qtdregpagina|
##    
##end

# CÓDGOS DO FLUXO DA TELA DE APROVAÇÃO DE REGISTROS
# CLICA NO BOTÃO APROVAÇÃO DA TELA TABELA DE PERDAS
Quando('clico no botao {string} e verifico que a tela aprovacao {string} foi aberta') do |btnome, aprovtela|
    #find('h3', text: "#{elementotela}", wait:5).visible?
    click_on(btnome, wait:10)
    find('h5', text: "#{aprovtela}", wait:10).visible?
end

Quando('clico no botao {string} e msg {string}') do |btnome, opertp|
    click_on(btnome)
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    find('p', text:"Não foi selecionado nenhum registro.", wait:5).visible?
end

Quando('clico no botao {string} da mensagem de erro da tela aprovacao') do |btnome|
    click_on(btnome)
end

Quando('clico no botao x para fechar a tela de aprovacao') do
    find("button[class='close']").click
end

# SELECIONA UM PRODUTO
Quando('seleciono um {string} parametro tela de aprovacao') do |produto|
    find("input[placeholder='Produto']").set(produto)
end

# SELECIONA O STATUS DO PRODUTO
##Quando('seleciono produto com {string} igual a pendente') do |statusprd|
##    find("input[placeholder='Produto']").set(produto)
##end

Quando('clico no checkbox de aprovacao do produto selecionado') do
    #first("input[class='ng-untouched ng-pristine ng-valid']").click
    @metodoserp.clicar_checkbox_aprov
end

Quando('clico no botao {string} dos parametros selecionados') do |btnome|
    click_on(btnome)
end
  
Quando('clico no botao {string} da mensagem de confirmacao da operacao {string}') do |btnome, opertp|
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
    msgaprovacao = find(:xpath, "//div/div/p").text
    #msgaprovacao = find('p', text:"Tem certeza que deseja aprovar os itens selecionados?", wait:5)
    find('p', text:"#{msgaprovacao}", wait:5).visible?
    click_on(btnome)
end

Quando('preencho o campo motivo {string} da negacao {string}') do |motivonega, opertp|
    find("input[placeholder='Digite o motivo']").set(motivonega)
    tirar_foto_allure("PrjTabPerdas-#{opertp}","TESTADO")
end

Quando('clico no botao {string} na tela de motivo de negacao') do |btnome|
    click_on(btnome)
end