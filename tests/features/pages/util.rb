class Util < SitePrism::Page
    def initialize
        # USUÁRIO QUE CADASTRA PARÂMETROS DE PERDAS
        #$usuario = "caroline_cerantola"
        #$senha = "WINTER123"
        # USUÁRIO QUE APROVA PARÂMETROS DA MATRIZ DE PERDAS
        $usuario = "michel_froio"
        $senha = "SENHA123"
        criar_pasta_log
    end

    def data_util
        data = Time.now
        if data.friday?
            datareturn = data.day + 3
        elsif data.saturday?
            datareturn = data.day + 2
        else
            datareturn = data.day + 1
        end
        
        return datareturn
    end

    def criar_pasta_log
        @diretorio = "C:/report_automacao"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        @diretorio = "#{Dir.pwd}/reports"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end
end