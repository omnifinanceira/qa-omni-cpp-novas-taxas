require "oci8"
require_relative 'util.rb'
require_relative "../support/helper.rb"

class MetodosErp < Util

    #set_url 'https://hml-omnifacil2.omni.com.br/hml/pck_login.prc_login'

    element :elementarquivo, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[3]/div[1]/input"
    element :elementcheck, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-table/div/div[2]/table/tbody/tr[1]/td[1]/input"
    element :elementcheckboxgp, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-grupo-perda-list/omni-table/div/div[2]/table/tbody/tr[1]/td[1]/input"
    element :elementchecktodos, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-table/div/div[2]/table/thead/tr[1]/th[1]/div/div/input"
    element :elementbuscar, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-table/div/div[1]/div[2]/div/input"
    ###element :elementeditar, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-table/div/div[2]/table/tbody/tr[1]/td[13]/button[2]"
    ###element :elementbtnOK, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-modal[1]/div[2]/div/div/div[2]/app-tabela-perda-form-container/app-tabela-perda/omni-modal/div[2]/div/div/div[3]/button"
    ###element :elementbtnfecharx, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-modal[1]/div[2]/div/div/div[1]/button"
    element :elementimportarq, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[3]/div[1]/input"
    element :elementcheckboxfa, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-fonte-analise-list/div[3]/div/omni-table/div/div[2]/table/tbody/tr/td[1]/input"
    element :elementochecboxaprov, :xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/omni-modal[2]/div[2]/div/div/div[2]/app-tabela-perda-aprovacao/div[2]/omni-table/div/div[2]/table/tbody/tr[1]/td[1]/input"

    def logar_usuario(usuario = $usuarioagente,senha = $senhaagente)
        elementinputusuario.set usuario
        elementinputsenha.set senha
        elementconectar.click
    end

    def selecionar_menu(menu = nil)
        if !menu.nil?
            sleep 2
            find(:xpath, "//*[@id='dropdownMenu_0'][text()=' #{menu} ']").click
        end
    end

    def selecionar_submenu(submenu = nil)
        if !submenu.nil?
            find('a', text: "#{submenu}").click
            ### Quando o usuário é marcos_cruz o nome do id é dropdownMenu_0_3
            ##find(:id, 'dropdownMenu_0_3', text: "#{submenu}").click
            ### Quando o usuário é caroline_cerantola o nome do id é dropdownMenu_0_0
            #find(:id, 'dropdownMenu_0_0', text: "#{submenu}").click
         end
    end
    
    ###def selecionar_subsubmenu1(subsubmenu1)
    ###    if !subsubmenu1.nil?
    ###        find('a', text: "#{subsubmenu1}").click
    ###    end
    ###end

    def selecionar_subsubmenu(subsubmenu = nil)
        if !subsubmenu.nil?
            if subsubmenu == "Tabela de Perda"
                ### o :xpath abaixo localiza qualquer opção de subsubmenu que tiver no código
                find(:xpath, "//li/ul/li/ul/li/a[contains(text(),'#{subsubmenu}')]").click
                ### ANOTAÇÕES ###
                #find('a', text: "#{subsubmenu}", match: :prefer_exact).click
                ##find('class', 'dropdown-submenu', text: "#{subsubmenu}")
                ##find(:xpath, "//[@class='dropdown-submenu']")
                ##find(:xpath, "//ul/li[@class='dropdown-submenu']")
            else
                find('a', text: "#{subsubmenu}").click
            end
        end
    end

    def espera_elemento(eletipo, eletexto)
        while find("#{eletipo}", text:"#{eletexto}").visible? == false
        end
    end

    def clicar_botao(btnome)
        click_on("#{btnome}")
    end

    # Clica no botão da Tela Tabela de Perda
    def clicar_nova_tabela_perda
        #while find('a', text:"Nova Tabela Perda").visible? == false
        #end
        #espera_elemento(eletipo = "a", eletexto = "Nova Tabela Perda")
        #espera_elemento(eletipo, eletexto)
        clicar_botao(btnome = "Nova Tabela Perda")
        #clicar_botao(btnome = eletexto)
        #click_on("Nova Tabela Perda")
        #elementnovatabelaperda.click
        #find("a[role='button']", text:" Nova Tabela Perda ")
        #find(:xpath, "//*[@id='produto']/div/div/div[2]")
        #find('a', ".class='btn btn-success btn-md'").click
        #find(:xpath, "//*[@id='single-spa-application:omni-parametro-agente-mf']/omni-parametro-agente-mf/section/div/app-tabelas-perdas-list/div[2]/a[1]")
    end
    
    # Clica no botão da Tela Grupo de Perda
    def clicar_nova_grupo_perda
        espera_elemento(eletipo = "a", eletexto = "Nova Grupo Perda")
        clicar_botao(btnome = "Nova Tabela Perda")
    end

    ### Retirado dia 11/02/2021
    ### def duplicidade_tela_btOK
    ###     elementbtnOK.click
    ###end

    ### Retirado dia 11/02/2021
    ###def btn_x_fechar_tela
    ###    elementbtnfecharx.click
    ###end

    # PREENCHE OS DADOS DOS CAMPOS DA TELA PARÂMETROS DE NOVAS TAXAS
    def preencher_parametros(produto, escinfer, escsuper, prazo, entinfer, entsuper, classagente, grupoperda, perda, fonteanalise)
        #find(:xpath, "//select[@id='p_grupo2']").find(:xpath, "option[@value='#{produto}']").select_option
        #find(:xpath, "//*[@id='a46bb33e3ffa-1']/span").find(:xpath, "//*[@id='produto']='#{produto}'").select_option
        #find(:xpath, "//*[@id='produto']/div/div/div[2]/span[2]")
        # Clica e seleciona dado na lista Produos
        find(:id, 'produto').click
        find(".ng-option-label", text:"#{produto}").click
        find(:id, 'minScore').set(escinfer)
        find(:id, 'maxScore').set(escsuper)
        find(:id, 'installments').set(prazo)
        find(:id, 'minInflowPercent').set(entinfer)
        find(:id, 'maxInflowPercent').set(entsuper)
        sleep 1
        find(:id, 'agentRating').click
        find(".ng-option-label", text:"#{classagente}").click
        sleep 1
        find(:id, 'lossGroup').click
        find(".ng-option-label", text:"#{grupoperda}").click
        find(:id, 'loss').set(perda)
        find(:id, 'sourceAnalysis').click
        sleep 1
        find_all(".ng-option-label")[fonteanalise.to_i].click
        ##tirar_foto_allure("1a_IncluirParametro", "TESTADO")
    end

    # ALTERA OS DADOS DOS CAMPOS DA TELA PARÂMETROS DE NOVAS TAXAS
    def alterar_dados_parametro(prazo, classagente, grupoperda, perda)
        #find(:id, 'produto').click
        #find(".ng-option-label", text:"#{produto}").click
        #find(:id, 'minScore').set(escinfer)
        #find(:id, 'maxScore').set(escsuper)
        find(:id, 'installments').set ""
        #prazo = "50"
        find(:id, 'installments').set(prazo)
        #find(:id, 'minInflowPercent').set(entinfer)
        #find(:id, 'maxInflowPercent').set(entsuper)
        sleep 1
        find(:id, 'agentRating').click
        #classagente = "AZUL"
        find(".ng-option-label", text:"#{classagente}").click
        sleep 1
        find(:id, 'lossGroup').click
        #grupoperda = "1 - Padrão"
        find(".ng-option-label", text:"#{grupoperda}").click
        find(:id, 'loss').set ""
        #perda = "11,0000"
        find(:id, 'loss').set(perda)
        #tirar_foto(nome_arquivo = "Teste_Foto_Novas_Taxas")
        #find(:id, 'sourceAnalysis').click
        #sleep 1
        #find_all(".ng-option-label")[fonteanalise.to_i].click
        ##tirar_foto_allure("2a_IncluirParametroDuplicidade-EditaCampos", "TESTADO")
    end

    ## Percebi que este código estava 
    ##def preencher_grupo_perda(codigo, descricao)
    ##    find(:id, 'id').set(codigo)
    ##    find(:id, 'description').set(descricao)
    ##end

    # Digita o produto no campo para Buscar o produto na matriz
    ### Retirado em 11/02/2022
    ###def selecionar_registros(produto)
    ###    elementbuscar.set("#{produto}")
    ###end

    # Clica no Check box para selecionar todos os registros da matriz
    ###def selecionar_todos_registros(regporpagina)
    ###    # códig xpath também funciona, mas deixei o css selector da linha seguinte
    ###    ##find(:xpath, "//option[text()='#{regporpagina}']").click
    ###    find('option', text: "#{regporpagina}").click
    ###    elementchecktodos.click
    ###    tirar_foto("6a_ExcluirParametro-SelecionarTodos", "TESTADO")
    ###end

    # MÉTODO É UTILIZADO NAS TRES TELAS. ELE ALTERA A PAGINAÇÃO DA CONSULTA NA TELA
    def selecionar_todos_registros(regporpagina)
        # código xpath também funciona, mas deixei o css selector da linha seguinte
        # voltei o xpath porque o option começou a dar ambiguos match
        find(:xpath, "//option[text()='#{regporpagina}']").click
        ###find('option', text: "#{regporpagina}").click
        first("input[class='ng-untouched ng-pristine ng-valid']").click
        ##tirar_foto_allure("6a_ExcluirParametro-SelecionarTodos", "TESTADO")
    end

    # CLICA NO CHECK BOX PARA SELECIONAR O REGISTRO DA MATRIZ
    def clicar_checkbox
        elementcheck.click
        ##tirar_foto_allure("5b_ExcluirParametro-ClicaCheckBox", "TESTADO")
    end

    # CLICA NO CHECK BOX DO REGISTRO SELECIONADO PARA SER EXCLUIDO NA MATRIZ GRUPO E PERDA
    def clicar_checkbox_gp
        ##first("input[class='ng-untouched ng-pristine ng-valid']")
        elementcheckboxgp.click
    end

    # CLICA NO CHECK BOX DO REGISTRO SELECIONADO PARA SER EXCLUIDO NA MATRIZ FONTE ANÁLISE
    def clicar_checkbox_fa
        elementcheckboxfa.click
    end

    def clicar_checkbox_aprov
        elementochecboxaprov.click
    end

    # PREENCHE OS DADOS DOS CAMPOS DA TELA GRUPO DE PERDA
    def preencher_grupo_perda(codigo, descricao)
        # Preenche dados nos campos da tela
        find(:id, 'id').set(codigo)  
        find(:id, 'description').set(descricao)
        ##tirar_foto_allure("IncluirRegistroGrupoPerda", "TESTADO")
    end

    def aguardar_carregando
        find(:xpath, "//div/span[@class='sr-only']']", wait:20).visible?
    end

    # CONSULTA REGISTRO NA MATRIZ TABELA DE PERDA UTILIZANDO O CÓDIGO DO REGISTRO
    def consulta_codigo_grupo_perda(codigo)
        find("input[placeholder='Código']").set(codigo)
    end

    # CONSULTA REGISTRO NA MATRIZ FONTE ANALISE UTILIZANDO O CÓDIGO DO REGISTRO
    # VER SE PODEMOS UNIFICAR COM AS CONSULTAS DA TABELA GRUPO DE PERDA
    def consulta_codigo_fonte_analise(codigo)
        #find("input[placeholder='Código']").set(codigo)
        find("input[placeholder='Descrição']").set(codigo)
    end

    # ALTERA OS DADOS DOS CAMPOS DA TELA GRUPO DE PERDA
    def altera_dados_grupo_perda(descricao)
        find(:id, 'description').set ""
        find(:id, 'description').set(descricao)
    end

    # PREENCHE DADOS NOS CAMPOS DA TELA FONTE ANÁLISE
    def preencher_fonte_analise(descricao, chave)
        find(:xpath, "//input[@placeholder='Digite a Descrição da Fonte análise']").set(descricao)
        find(:xpath, "//input[@placeholder='Digite a Chave da Fonte análise']").set(chave)
        ##tirar_foto_allure("IncluirRegistroFonteAnalise", "TESTADO")
    end

    # ALTERA OS DADOS DOS CAMPOS DA TELA DE FONTE ANÁLISE
    def altera_dados_fonte_analise(descricao, chave)
        find(:xpath, "//input[@placeholder='Digite a Descrição da Fonte análise']").set ""
        find(:xpath, "//input[@placeholder='Digite a Descrição da Fonte análise']").set(descricao)
        find(:xpath, "//input[@placeholder='Digite a Chave da Fonte análise']").set ""
        find(:xpath, "//input[@placeholder='Digite a Chave da Fonte análise']").set(chave)
    end

end