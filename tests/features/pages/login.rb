require_relative 'util.rb'

class Login < Util

    set_url '/login#'

    element :elementinputusuario, :xpath, "//input[@id='txtUsuario']"
    element :elementinputsenha, :xpath, "//input[@id='txtSenha']"
    #element :elementspanentrar, :xpath, "//a/span[text()='Entrar']"
    element :elementbuttonentrar, :xpath, "//*[@id='login-container']/div/form/button"
    #element :elementbuttonconfirmar, :xpath, "//div/button[text()='Confirmar']"

    
    def preencher_usuario(usuario)
        elementinputusuario.set usuario
    end

    def preencher_senha(senha)
        elementinputsenha.set senha
    end

    def clicar_entrar
        elementbuttonentrar.click
    end

    def acessar_omni_erp
        preencher_usuario($usuario)
        preencher_senha($senha)
        clicar_entrar
    end
    
    #//*[@id="login-container"]/div/form/div[1]

end