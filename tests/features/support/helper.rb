require 'date'
require 'fileutils'
require 'rubygems'
require 'allure-cucumber'

########################################################################
### PARA RODAR O RELATÍRO NO ALLURE
# Abrir o aplicativo CMeder ou o CMD e colcar o comando abaixo:
# allure serve C:\WorkspaceVsj\qa-omni-cpp-novas-taxas\tests\reports
########################################################################

module Helper

   def tirar_foto(nome_arquivo, resultado)
      caminho_arquivo = "results/screenshots/test_#{resultado}"
      foto = "#{caminho_arquivo}/#{nome_arquivo}.png"
      attach(page.save_screenshot(foto), 'image/png')
   end
   
   ### EM 22/04/2022 IMPLEMENTADO ESTE MÉTODO MAS FOI COMENTADO POIS NÃO FUNCIONOU.
   #def take_screenshot(nome_arquivo, resultado)
   def tirar_foto_allure(nome_arquivo, resultado)
       caminho_arquivo_1 = "C:/results/screenshots/teste_#{resultado}"
       foto = "#{caminho_arquivo_1}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"
       
       caminho_arquivo = "C:/results/screenshots"
       foto = "#{caminho_arquivo}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"
       ##foto = "#{caminho_arquivo}/#{nome_arquivo}.png"
       attach(page.save_screenshot(foto), 'image/png')
       Allure.add_attachment(
          name: "Attachment",
          source: File.open("#{foto}"),
          type: Allure::ContentType::PNG,
          test_case: true
       ) 
   end

   def verificar_url
      if current_url.include? CONFIG['url_padrao']
      else
          binding.pry
      end
  end

   ###def tirar_foto_1(nome_arquivo, resultado)
   ###   ##caminho_arquivo = "results/screenshots"
   ###   ##foto = "#{caminho_arquivo}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"
   ###   ##page.save_screenshot(foto)
###
   ###   caminho_arquivo = "C:/results/screenshots"
   ###   #foto = "#{caminho_arquivo}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"
   ###   foto = "#{caminho_arquivo}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"
   ###   page.save_screenshot(foto)
   ###end

end